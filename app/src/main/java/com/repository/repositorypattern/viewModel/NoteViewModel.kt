package com.repository.repositorypattern.viewModel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.repository.repositorypattern.repository.NoteRepository
import com.repository.repositorypattern.room.entity.Note
import kotlinx.coroutines.launch

class NoteViewModel(application: Application) : AndroidViewModel(application) {

    private var repository = NoteRepository(application)
    private lateinit var allNotes: MutableLiveData<List<Note>>

    fun insert(note: Note) {
        viewModelScope.launch {
            repository.insert(note)
        }
    }

    fun update(note: Note) {
        viewModelScope.launch {
            repository.update(note)
        }
    }

    fun delete(note: Note) {
        viewModelScope.launch {
            repository.delete(note)
        }
    }

    fun deleteAllNotes() {
        viewModelScope.launch {
            repository.deleteAllNotes()
        }
    }

    fun getAllNotes(): LiveData<List<Note>> {
        viewModelScope.launch {
            allNotes.value = repository.getAllNotes().value
        }
        return allNotes
    }

}