package com.repository.repositorypattern.repository

import android.app.Application
import com.repository.repositorypattern.room.dao.NoteDao
import com.repository.repositorypattern.room.database.NoteDatabase
import com.repository.repositorypattern.room.entity.Note

class NoteRepository(application: Application) {

    private var noteDao: NoteDao = NoteDatabase(application).noteDao()

    suspend fun insert(note: Note) {
        noteDao.insert(note)
    }

    suspend fun update(note: Note) {
        noteDao.update(note)
    }

    suspend fun delete(note: Note){
        noteDao.delete(note)
    }

    suspend fun deleteAllNotes() {
        noteDao.deleteAllNotes()
    }

    suspend fun getAllNotes() =  noteDao.getAllNotes()

}